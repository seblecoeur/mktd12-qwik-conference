import { component$ } from '@builder.io/qwik';
import { routeLoader$ } from '@builder.io/qwik-city';
import {Speaker, SpeakerComponent} from "~/components/speaker";

export const useSpeakersList = routeLoader$(async () => {
  // This code runs only on the server, after every navigation
  const res = await fetch(`https://konfetti.monkeypatch.io/web/conferences/demo-konfetti-2023/speakers`);
  const speakers = await res.json();
  return speakers as Speaker[];
});
 
export default component$(() => {
  // In order to access the `routeLoader$` data within a Qwik Component, you need to call the hook.
  const signal = useSpeakersList(); // Readonly<Signal<Product>>
  return (
   <>
     <div class="container py-10">

    <h1>Speakers</h1>
       <div class="grid grid-cols-4 gap-8">

    {signal.value.map(speaker => <SpeakerComponent key={speaker.id} speaker={speaker} />)}
       </div>
     </div>
   </>);
});