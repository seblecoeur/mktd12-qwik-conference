import {
  component$,
  useSignal,
  useStylesScoped$,
  useVisibleTask$,
} from "@builder.io/qwik";

interface Prop {
  paths: string[];
}

export const Carousel = component$((prop: Prop) => {
  const imageNumber = useSignal(0);
  useVisibleTask$(() => {
    setInterval(() => {
      imageNumber.value++;
      if (imageNumber.value > prop.paths.length - 1) {
        imageNumber.value = 0;
      }
    }, 3000);
  });

  useStylesScoped$(`
    button {
      @apply bg-blue-500
    }
  `);

  return (
    <>
      <div class="container">
        <h2>Edition passée</h2>
        <div class="grid grid-cols-3">
          <div class="col-span-2">
            <div class="overflow-hidden rounded-3xl">
              <img
                class="h-[300px] w-full object-cover"
                src={prop.paths[imageNumber.value]}
                alt="toto"
              />
            </div>
            <div class="space-x-3">
              <button
                onClick$={() => {
                  if (imageNumber.value > 0) {
                    imageNumber.value--;
                  } else {
                    imageNumber.value = prop.paths.length - 1;
                  }
                }}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  fill="currentColor"
                  class="h-6 w-6"
                >
                  <path d="M9.195 18.44c1.25.714 2.805-.189 2.805-1.629v-2.34l6.945 3.968c1.25.715 2.805-.188 2.805-1.628V8.69c0-1.44-1.555-2.343-2.805-1.628L12 11.029v-2.34c0-1.44-1.555-2.343-2.805-1.628l-7.108 4.061c-1.26.72-1.26 2.536 0 3.256l7.108 4.061Z" />
                </svg>
              </button>
              <button
                onClick$={() => {
                  if (imageNumber.value < prop.paths.length - 1) {
                    imageNumber.value++;
                  } else {
                    imageNumber.value = 0;
                  }
                }}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  fill="currentColor"
                  class="h-6 w-6"
                >
                  <path d="M5.055 7.06C3.805 6.347 2.25 7.25 2.25 8.69v8.122c0 1.44 1.555 2.343 2.805 1.628L12 14.471v2.34c0 1.44 1.555 2.343 2.805 1.628l7.108-4.061c1.26-.72 1.26-2.536 0-3.256l-7.108-4.061C13.555 6.346 12 7.249 12 8.689v2.34L5.055 7.061Z" />
                </svg>
              </button>
            </div>
          </div>
          <div></div>
        </div>
      </div>
    </>
  );
});
