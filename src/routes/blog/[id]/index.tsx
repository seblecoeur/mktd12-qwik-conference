import { component$ } from "@builder.io/qwik";
import { type DocumentHead, routeLoader$ } from "@builder.io/qwik-city";
import { routeAction$, Form } from "@builder.io/qwik-city";
import { createSubConnection } from "~/utils";

export const usePost = routeLoader$(async (requestEv) => {
  const supabaseClient = createSubConnection(requestEv);
  const { data } = await supabaseClient
    .from("BlogPost")
    .select()
    .eq("id", requestEv.params.id)
    .single();

  return { data };
});

export const usePostComments = routeLoader$(async (requestEv) => {
  const supabaseClient = createSubConnection(requestEv);
  const { data } = await supabaseClient
    .from("BlogPostComment")
    .select()
    .eq("blogPostId", requestEv.params.id)
    .order("created_at", { ascending: false });

  return { data };
});

export const useAddPostComment = routeAction$(async (data, requestEvent) => {
  const supabaseClient = createSubConnection(requestEvent);
  await supabaseClient
    .from("BlogPostComment")
    .insert({ blogPostId: requestEvent.params.id, content: data.content });

  return {
    success: true,
  };
});

export default component$(() => {
  const post = usePost();
  const action = useAddPostComment();
  const postComments = usePostComments();

  return (
    <>
      <div class="relative">
        <img
          width="1280"
          height="960"
          class=" max-h-[300px] w-full object-cover"
          src={post.value.data.thumbnail}
        />

        <h1 class="absolute bottom-8 left-4 text-white">
          {post.value.data.title}
        </h1>
      </div>
      <div class="mx-auto mt-2 max-w-[800px]">
        <p>{post.value.data.body}</p>
        <div>
          <h3 class="underline underline-offset-1">Commentaires :</h3>
          {postComments.value.data!.map(({ id, content, created_at }) => (
            <div
              key={id}
              class="border-grey relative mt-2 min-h-[50px] border-t-[1px] border-solid"
            >
              <p>{content}</p>
              <p class="absolute bottom-0 left-0">
                {new Date(created_at).toDateString()}
              </p>
            </div>
          ))}
          <Form
            action={action}
            class="container border border-t-[2px] border-dotted p-5"
          >
            <input
              type="text"
              name="content"
              placeholder="Contenu du commentaire"
            />
            <button
              class="rounded-md bg-indigo-500 px-4 py-2 text-white"
              type="submit"
            >
              Ajouter
            </button>
          </Form>
        </div>
        {action.value?.success && <p>Commentaire ajouté avec success</p>}
      </div>
    </>
  );
});

export const head: DocumentHead = {
  title: "Welcome to my blog post",
  meta: [
    {
      name: "post blog",
      content: "Qwik site description",
    },
  ],
};
