import {component$} from '@builder.io/qwik';
import {routeLoader$} from "@builder.io/qwik-city";
import {SpeakerComponent} from "~/components/speaker";
import type {Speaker} from "~/components/speaker";

interface Room {
    key: string;
    name: string;
    description: string;
}

interface Tag {
    id: string;
    name: string;
}

interface Session {
    id: string;
    type: string;
    title: string;
    description: string;
    pictureUrl: string;
    speakers?: [Speaker];
    language?: string;
    tags?: [Tag];

}

interface SessionResponse {
    period: {
        start_date: string;
        end_date: string;
    };
    room: Room;
    session: Session;
}

export const useTalk = routeLoader$(async (requestEvent) => {
    const talkId = requestEvent.params.id;

    const response = await fetch(`https://konfetti.monkeypatch.io/web/conferences/demo-konfetti-2023/sessions/${talkId}`);
    if (response.status != 200) {
        return requestEvent.fail(500, {
            error: `Unexpected error: ${response.status}`
        });
    }
    const json = await response.json();
    return json as SessionResponse;
});

export default component$(() => {
    const talk = useTalk();

    if (talk.value.error) {
        return <div>Unexpected error: {talk.value.error}</div>;
    }
    return <div class="animated_page">
        <h1 class="talk_title">{talk.value.session!.title}</h1>
        <p class="talk_period">Le {new Date(talk.value.period!.start_date).toDateString()} de {new Date(talk.value.period!.start_date).toLocaleTimeString()} à {new Date(talk.value.period!.end_date).toLocaleTimeString()}</p>
        {talk.value.session!.speakers &&
            <div>{talk.value.session!.speakers.map(s => <SpeakerComponent key={s.id} speaker={s}/>)}</div>
        }
        <p class="talk_description">{talk.value.session!.description}</p>
        {talk.value.session!.tags &&
            <p class="talk_tags">{talk.value.session!.tags.map(t => "#" + t.name).join(" ")}</p>
        }
    </div>;
});
