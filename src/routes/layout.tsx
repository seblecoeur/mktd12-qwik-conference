import { component$, Slot } from "@builder.io/qwik";
import { Link, RequestHandler } from "@builder.io/qwik-city";

export const onGet: RequestHandler = async ({ cacheControl }) => {
  // Control caching for this request for best performance and to reduce hosting costs:
  // https://qwik.builder.io/docs/caching/
  cacheControl({
    // Always serve a cached response by default, up to a week stale
    staleWhileRevalidate: 60 * 60 * 24 * 7,
    // Max once every 5 seconds, revalidate on the server to get a fresh version of this page
    maxAge: 5,
  });
};

export default component$(() => {
  return (
    <>
      <header class=" relative z-10 bg-gray-900 p-4 shadow">
        <ul class="container flex gap-8 text-white">
          <li>
            <Link href="/">HOME</Link>
          </li>
          <li>
            <Link href="/speaker">SPEAKERS</Link>
          </li>
          <li>
            <Link href="/planning">PLANNING</Link>
          </li>
          <li>
            <Link href="/blog">BLOG</Link>
          </li>
        </ul>
      </header>
      <main class="min-h-screen space-y-40">
        <Slot />
      </main>
      <footer class="bg-violet-500 p-3 mt-10 text-white">
        <div class="container text-center">© MKTD Qwik vs Next</div>
      </footer>
    </>
  );
});
