import {component$} from "@builder.io/qwik";

interface Prop {
    paths: string[]
}

export const Sponsor = component$((prop: Prop) => {
    return (<>
        <div class="flex justify-evenly py-5">

        {prop.paths.map((p) => (<div class="w-20 h-20 grayscale overflow-visible"><img src={p} class="w-full h-full object-contain"/></div>))}
        </div>
    </>)
})