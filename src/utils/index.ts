import {createServerClient} from "supabase-auth-helpers-qwik"
import {RequestEventCommon} from "@builder.io/qwik-city/middleware/request-handler";

export const createSubConnection = (requestEv: RequestEventCommon) => {
    const supabaseClient = createServerClient(
        requestEv.env.get("PUBLIC_SUPABASE_URL")!,
        requestEv.env.get("PUBLIC_SUPABASE_ANON_KEY")!,
        requestEv
    )

    return supabaseClient
}
