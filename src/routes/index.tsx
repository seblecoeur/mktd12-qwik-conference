import {component$} from "@builder.io/qwik";
import type {DocumentHead} from "@builder.io/qwik-city";
import {Carousel} from "~/components/carousel/carousel";
import {Sponsor} from "~/components/sponso/sponso";

export default component$(() => {
    return (
        <>
            <section class="flex items-center h-[500px] gap-7 bg-gradient-to-r from-cyan-500 to-blue-500 text-white">
                <div class="container grid grid-cols-3 gap-5">

                    <div class="flex flex-col items-center justify-center">
                        <div class="space-y-2">

                            <h1 class="">Monkey Tech Days</h1>
                            <p class="text-3xl">C'est génial houlala</p>
                        </div>
                    </div>
                    <div class="col-span-2 flex items-center">
                        <div class="rounded-3xl overflow-hidden mt-40">
                            <img src="hero.jpg" alt="" class="object-cover w-full h-full"/>
                        </div>
                    </div>
                </div>
            </section>
            <Carousel paths={["monkey1.jpg", "monkey2.jpg"]}/>
            <section>
                <div class="container">
                    <h2 class="text-center">Sponsors</h2>
                    <Sponsor
                        paths={['sponsors/Bollore.png',
                            'sponsors/CIA.png',
                            'sponsors/Nextjs.png',
                            'sponsors/StadeToulousain.png',
                            'sponsors/TotalEnergies.png']}/>
                </div>
            </section>
        </>
    );
});

export const head: DocumentHead = {
    title: "Welcome to Qwik",
    meta: [
        {
            name: "description",
            content: "Qwik site description",
        },
    ],
};
