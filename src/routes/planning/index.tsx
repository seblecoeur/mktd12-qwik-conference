import { component$ } from "@builder.io/qwik";
import { routeLoader$ } from "@builder.io/qwik-city";
import style from "./planning.module.css";

export interface Room {
  key: string;
  name: string;
  description: string;
}
export interface Periode {
  start_date: string;
  end_date: string;
}
export interface Session {
  type: string;
  id: string;
  title: string;
  description: string;
  pictureUrl: string;
  speakers: Speaker[];
  tags: Tag[];
  language: string;
}
export interface Tag {
  id: string;
  name: string;
}
export interface Speaker {
  id: string;
  full_name: string;
  company: string;
  avatar_url: string;
}
export interface ScheduleItem {
  period: Periode;
  room: Room;
  session: Session;
}
export interface Planning {
  rooms: Room[];
  items: ScheduleItem[];
}

export const fetchPlanning = async () => {
  const res = await fetch(
    `https://konfetti.monkeypatch.io/web/conferences/demo-konfetti-2023/schedule`,
  );
  const product = await res.json();
  return product as Planning;
};

export const usePlanning = routeLoader$(async () => {
  return fetchPlanning();
});

export default component$(() => {
  const planning = usePlanning();
  return (
    <div class="container">
      <div class={style.planningTitle}> Planning :</div>
      <div class={style.planning}>
        <div class={style.cardRoom}>
          <h3 class={style.titleRoom}>Main room :</h3>
          <div>
            {planning.value.items
              .filter((sceduleItem) => sceduleItem.room.key == "main-room")
              .map((sceduleItem, index) => {
                return <SceduleCard key={index} {...sceduleItem} />;
              })}
          </div>
        </div>
        <div class={style.cardRoom}>
          <h3 class={style.titleRoom}>Second room :</h3>
          <div>
            {planning.value.items
              .filter((sceduleItem) => sceduleItem.room.key == "second-room")
              .map((sceduleItem, index) => {
                return <SceduleCard key={index} {...sceduleItem} />;
              })}
          </div>
        </div>
      </div>
    </div>
  );
});

function SceduleCard(sceduleItem: ScheduleItem) {
  return (
    <div class={style.scheduleCard}>
      <div>
        <div class={style.date}>
          {sceduleItem.period.start_date.slice(11, 16)}
        </div>
        <p>To </p>
        <div class={style.date}>
          {sceduleItem.period.end_date.slice(11, 16)}
        </div>
      </div>
      <a class={style.schedule} href={"/talk/" + sceduleItem.session.id}>
        <div class={style.header}>
          <h3 class={style.titleSchedule}>{sceduleItem.session.title}</h3>
          <div>{sceduleItem.session.language}</div>
        </div>{" "}
        <div class={style.speakerAndTag}>
          <div>
            {sceduleItem.session.speakers?.map((speaker, index) => {
              return <Speaker key={index} {...speaker} />;
            })}
          </div>
          <div>
            {sceduleItem.session.tags?.map((tag, index) => {
              return <Tag key={index} {...tag} />;
            })}
          </div>
        </div>
      </a>
    </div>
  );
}
function Speaker(speaker: Speaker) {
  return (
      <div class="container">

    <div class={style.speakerCard}>
      <div class={style.avatar}>
        <Avatar url={speaker.avatar_url} />
      </div>
      <div>
        <p class={style.speakerName}>{speaker.full_name}</p>
        <p class={style.speakerCompany}>{speaker.company}</p>
      </div>
    </div>
      </div>
  );
}

function Avatar({ url }: {url: any;}) {
  return (
    <img
      src={url}
      alt="Avatar"
      style={{
        width: "20px",
        height: "20px",
        borderRadius: "50%",
      }}
    />
  );
}

function Tag(tag: Tag) {
  return <div class={style.tag}>{tag.name}</div>;
}
