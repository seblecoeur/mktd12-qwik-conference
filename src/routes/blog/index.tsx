import { component$ } from "@builder.io/qwik";
import { routeLoader$, type DocumentHead } from "@builder.io/qwik-city";
import { createSubConnection } from "~/utils";

export const usePosts = routeLoader$(async (requestEv) => {
  const supabaseClient = createSubConnection(requestEv);
  const { data } = await supabaseClient.from("BlogPost").select("*");
  return { data };
});

export default component$(() => {
  const posts = usePosts();

  return (
    <>
      <div class="mx-auto max-w-[800px]">
        <h1 class="text-2xl">Blog</h1>
        {posts.value.data!.map(
          ({ id, title, abstract, thumbnail, date, author }) => (
            <a
              class="border-grey mx-2 my-4 flex max-w-[700px] border-[1px] border-solid "
              href={`${id}`}
              key={id}
            >
              <img
                class="object-cover"
                src={thumbnail}
                width="128"
                height="128"
              />

              <div class="flex flex-col justify-between p-4">
                <div>
                  <h2 class="text-xl">{title}</h2>
                  <p class="text-slate-500">{abstract}</p>
                </div>
                <div class="mt-2 flex justify-between text-slate-500">
                  <span class="date">{date}</span>
                  <span class="author">{author}</span>
                </div>
              </div>
            </a>
          ),
        )}
      </div>
    </>
  );
});

export const head: DocumentHead = {
  title: "Blog",
  meta: [
    {
      name: "blog",
      content: "Blog description",
    },
  ],
};
